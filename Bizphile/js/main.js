(function () {
  // Polyfill for NodeList.prototype.forEach() in IE
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;
      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  }

  // Variables
  var nav = document.querySelector('.header__navigation');
  var langSwitcher = document.querySelector('.header__language-switcher');
  var search = document.querySelector('.header__search');
  var allToggles = document.querySelectorAll('.header--toggle');
  var navToggle = document.querySelector('.header__navigation--toggle');
  var langToggle = document.querySelector('.header__language-switcher--toggle');
  var searchToggle = document.querySelector('.header__search--toggle');
  var closeToggle = document.querySelector('.header__close--toggle');
  var allElements = document.querySelectorAll(
    '.header--element, .header--toggle'
  );
  var emailGlobalUnsub = document.querySelector('input[name="globalunsub"]');

  // Functions

  // Function for executing code on document ready
  function domReady(callback) {
    if (['interactive', 'complete'].indexOf(document.readyState) >= 0) {
      callback();
    } else {
      document.addEventListener('DOMContentLoaded', callback);
    }
  }

  // Function for toggling mobile navigation
  function toggleNav() {
    allToggles.forEach(function (toggle) {
      toggle.classList.toggle('hide');
    });

    nav.classList.toggle('open');
    navToggle.classList.toggle('open');

    closeToggle.classList.toggle('show');
  }

  // Function for toggling mobile language selector
  function toggleLang() {
    allToggles.forEach(function (toggle) {
      toggle.classList.toggle('hide');
    });

    langSwitcher.classList.toggle('open');
    langToggle.classList.toggle('open');

    closeToggle.classList.toggle('show');
  }

  // Function for toggling mobile search field
  function toggleSearch() {
    allToggles.forEach(function (toggle) {
      toggle.classList.toggle('hide');
    });

    search.classList.toggle('open');
    searchToggle.classList.toggle('open');

    closeToggle.classList.toggle('show');
  }

  // Function for the header close option on mobile
  function closeAll() {
    allElements.forEach(function (element) {
      element.classList.remove('hide', 'open');
    });

    closeToggle.classList.remove('show');
  }

  // Function to disable the other checkbox inputs on the email subscription system page template
  function toggleDisabled() {
    var emailSubItem = document.querySelectorAll('#email-prefs-form .item');

    emailSubItem.forEach(function (item) {
      var emailSubItemInput = item.querySelector('input');

      if (emailGlobalUnsub.checked) {
        item.classList.add('disabled');
        emailSubItemInput.setAttribute('disabled', 'disabled');
        emailSubItemInput.checked = false;
      } else {
        item.classList.remove('disabled');
        emailSubItemInput.removeAttribute('disabled');
      }
    });
  }

  // Execute JavaScript on document ready
  domReady(function () {
    if (!document.body) {
      return;
    } else {
      // Function dependent on language switcher
      if (langSwitcher) {
        langToggle.addEventListener('click', toggleLang);
      }

      // Function dependent on navigation
      if (navToggle) {
        navToggle.addEventListener('click', toggleNav);
      }

      // Function dependent on search field
      if (searchToggle) {
        searchToggle.addEventListener('click', toggleSearch);
      }

      // Function dependent on close toggle
      if (closeToggle) {
        closeToggle.addEventListener('click', closeAll);
      }

      // Function dependent on email unsubscribe from all input
      if (emailGlobalUnsub) {
        emailGlobalUnsub.addEventListener('change', toggleDisabled);
      }
    }
  });
})();


$(document).ready(function(){

	$('.trMenurow').before('<a class="menuToggle flex ffRowwrap justCenter alignCenter"><i></i></a>');
	$('.hs-menu-children-wrapper').before('<a class="childToggle flex ffRowwrap justCenter alignCenter"><i></i></a>');
	
	$('.menuToggle').on('click',function(){
		$('body').toggleClass('menuOpen');
		$(this).next('.trMenurow').slideToggle(200);
		$('.trMenurow ul.hs-menu-children-wrapper').slideUp(200);
		$('.trMenurow li.hs-item-has-children').removeClass('childOpen');
	});
	
	$('.childToggle').on('click',function(){
		$(this).parent().siblings().removeClass('childOpen');
		$(this).parent().siblings().find('li').removeClass('childOpen');
		$(this).parent().siblings().find('ul').slideUp(200);
		$(this).parent().toggleClass('childOpen');
		$(this).next('ul').slideToggle(200);
	});
	
	$('.trsIcon').on('click',function(e){
		e.stopPropagation();
		$(this).parent().toggleClass('active');
	});
	
	$('.trSearch').on('click',function(e){
			e.stopPropagation();
	});
	
	$('body').on('click',function(){
		$('.trSearch').removeClass('active');
	});
	
	
	$('.heroSec').slick({
  dots: false,
	arrows: true,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
	autoplay: true,
  autoplaySpeed: 4000,
	speed: 300,
		responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
      }
    }
 ]
});


$('.whatwdRow').slick({
  dots: false,
	arrows:false,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 601,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
	
	$('.whatwdLogos').slick({
  dots: false,
	arrows:false,
  infinite: true,
  autoplay: true,
  autoplaySpeed: 2500,
  speed: 1000,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1261,
      settings: {
        slidesToShow: 4
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }
    },
		{
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 601,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
	
 $('.teamRow').slick({
  dots: false,
	arrows:false,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1261,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
	
	
	$('.insightRow').slick({
  dots: false,
	arrows:false,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2       
      }
    },
    {
      breakpoint: 601,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
	
	
	 $('.testimonialRow').slick({
  dots: false,
	arrows:false,
  infinite: true,
  speed: 300,
  slidesToShow: 2,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1261,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 1
      }
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
	
		 $('.news-crnlft').slick({
  dots: false,
	arrows:false,
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 1
      }
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});
 	
// 	$(document).on( 'scroll', function(){
// 		if($('.animSec').hasClass('active')){
			/* Counter Code */
			 (function($) {
			  $.fn.countTo = function(options) {
				options = options || {};

				return $(this).each(function() {
					// set options for current element
					var settings = $.extend(
						{},
						$.fn.countTo.defaults,
						{
							from: $(this).data("from"),
							to: $(this).data("to"),
							speed: $(this).data("speed"),
							refreshInterval: $(this).data("refresh-interval"),
							decimals: $(this).data("decimals")
						},
						options
					);

					// how many times to update the value, and how much to increment the value on each update
					var loops = Math.ceil(settings.speed / settings.refreshInterval),
						increment = (settings.to - settings.from) / loops;

					// references & variables that will change with each update
					var self = this,
						$self = $(this),
						loopCount = 0,
						value = settings.from,
						data = $self.data("countTo") || {};

					$self.data("countTo", data);

					// if an existing interval can be found, clear it first
					if (data.interval) {
						clearInterval(data.interval);
					}
					data.interval = setInterval(updateTimer, settings.refreshInterval);

					// initialize the element with the starting value
					render(value);

					function updateTimer() {
						value += increment;
						loopCount++;

						render(value);

						if (typeof settings.onUpdate == "function") {
							settings.onUpdate.call(self, value);
						}

						if (loopCount >= loops) {
							// remove the interval
							$self.removeData("countTo");
							clearInterval(data.interval);
							value = settings.to;

							if (typeof settings.onComplete == "function") {
								settings.onComplete.call(self, value);
							}
						}
					}

					function render(value) {
						var formattedValue = settings.formatter.call(self, value, settings);
						$self.html(formattedValue);
					}
				});
			};

		$.fn.countTo.defaults = {
			from: 0, // the number the element should start at
			to: 0, // the number the element should end at
			speed: 1000, // how long it should take to count between the target numbers
			refreshInterval: 100, // how often the element should be updated
			decimals: 0, // the number of decimal places to show
			formatter: formatter, // handler for formatting the value before rendering
			onUpdate: null, // callback method for every time the element is updated
			onComplete: null // callback method for when the element finishes updating
		};

		function formatter(value, settings) {
			return value.toFixed(settings.decimals);
		}
	})(jQuery);

		jQuery(function($) {
			// custom formatting example
			$(".count-number").data("countToOptions", {
				formatter: function(value, options) {
					return value
						.toFixed(options.decimals)
						.replace(/\B(?=(?:\d{3})+(?!\d))/g, "");
				}
			});

			// start all the timers
			$(".timer").each(count);

			function count(options) {
				var $this = $(this);
				options = $.extend({}, options || {}, $this.data("countToOptions") || {});
				$this.countTo(options);
			}
		});
 	
	
			/* Progress Bar */
			var bars = document.querySelectorAll('.goalscls .goalsclsLine');
			console.clear();

			setInterval(function(){
				bars.forEach(function(bar){
					var getWidth = parseFloat(bar.dataset.progress);

					for(var i = 0; i < getWidth; i++) {
						bar.style.width = i + '%';
					}
				});
			}, 500);
//  }
// });
	
	
	$(function() {
  $('.faqClsttl , .news-crnRcls h4, .row-fluid .faqCls .faqClsbox ').click(function(j) {
    $(this).next().slideToggle('.faqClstxt , .ews-crnRctxt');
   $(this).toggleClass('active');
    j.preventDefault();
  });
		$( ".faqCls:first-child .faqClsttl , .news-crnRcls:first-child h4 , .row-fluid .faqCls:before  " ).trigger( "click" );
});
	
	
		function animp(){
			$('.animSec').each(function(){
				var top_of_element = $(this).offset().top;
				var bottom_of_element = $(this).offset().top + $(this).outerHeight();
				var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
				var top_of_screen = $(window).scrollTop();
				if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
					 $(this).addClass('active');
				} else {

				}
			});
      }
		$(window).scroll(function() {
				animp();
		});

	
	
	$(".bizblg-share").click(function(e){
		e.preventDefault();
  $(".bizBlog-sml").toggleClass("open-socialmedia");
});
	
	$('[data-fancybox]').fancybox({
		// Options will go here
		buttons : [
			"fullScreen",
			"zoom",
			"share",
			 "close",
	//     "slideShow",
	//     "download",
	//     "thumbs"

		],
		closeClick  : false,
		wheel : false,
		transitionEffect: "slide",
		 // thumbs          : false,
		// hash            : false,
			 loop : true,
		// keyboard        : true,
		toolbar : true,
		// animationEffect : false,
		// arrows          : true,
		clickContent : true,

	});
	
	
});















